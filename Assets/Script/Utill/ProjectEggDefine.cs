﻿using UnityEngine;

namespace ProjectEgg.Util
{
    public enum Type_Prob
    {
        Body = 1,
        LeftEye = 2,
        RightEye = 3,
        Mouse = 4,
        Face = 5,
        SmallHat = 6,
        BigHat = 7,
    }
    public class ProjectEggDefine
    {
        public const string BUILD_PATH = "BUILD_PATH";
        public const string SCREENSHOT_PATH = "SCREENSHOT_PATH";
        public const string KEY_STORE_NAME = "KEY_STORE_NAME";
        public const string KEY_STORE_PASSWORD = "KEY_STORE_PASSWORD";
        public const string KEY_ALIAS = "KEY_ALIAS";
        public const string KEY_ALIAS_PASSWORD = "KEY_ALIAS_PASSWORD";
        public const string PACKAGE_NAME = "PACKAGE_NAME";
        public const string DEVELOPMENT_BUILD = "DEVELOPMENT_BUILD";
    }
}