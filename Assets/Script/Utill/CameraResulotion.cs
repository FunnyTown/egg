﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PJSnow
{
    /// <summary>
    /// 카메라 해상도 대응
    /// </summary>
    public class CameraResulotion : MonoBehaviour
    {
        private void Start()
        {
            CanvasScaler Scaler = GetComponent<CanvasScaler>();
            if (null == Scaler)
                return;

            float WHvalue = (float)Screen.width / Screen.height;
            float value_1 = 2 - (float)WHvalue;
            float value_2 = value_1 / 0.7f;
            float result = 1 - value_2;

            if (result <= 0.7f)
                result = 0;
            else
                result = 1;

            Scaler.matchWidthOrHeight = result;
        }
    }
}


