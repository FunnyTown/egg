﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public static class Extention
{
    /// <summary>
    /// Child Find by name
    /// </summary>
    /// <param name="root">parent GameObject</param>
    /// <param name="str">Object name</param>
    /// <returns>resutl GameObject</returns>
    public static GameObject GetChildObject(this GameObject root, string str)
    {
        if (root.name == str)
        {
            return root;
        }

        Transform trans = root.GetComponent<Transform>(str);
        if (null != trans)
            return trans.gameObject;

        return null;
    }

    public static GameObject GetChildActiveObject(this GameObject root, string str)
    {
        if (root.name == str)
        {
            return root.gameObject;
        }

        Transform trans = root.GetComponentInActive<Transform>(str);
        if (null != trans)
            return trans.gameObject;

        return null;
    }

    public static T GetComponentInActive<T>(this GameObject root, string str) where T : Component
    {
        if (root.name.Equals(str))
            return root.GetComponent<T>();

        List<T> comps = new List<T>(root.GetComponentsInChildren<T>(false));
        return comps.Find(d => d.name.Equals(str) == true);
    }

    public static GameObject GetChildObject(this Transform root, string str)
    {
        if (root.name == str)
        {
            return root.gameObject;
        }

        Transform trans = root.GetComponent<Transform>(str);
        if (null != trans)
            return trans.gameObject;

        return null;
    }

    /// <summary>
    /// Child Find by name
    /// </summary>
    /// <typeparam name="T">Component Class</typeparam>
    /// <param name="root">parent GameObject</param>
    /// <param name="str">Object name</param>
    /// <returns></returns>
    public static T GetComponent<T>(this GameObject root, string str) where T : Component
    {
        if (root.name.Equals(str))
            return root.GetComponent<T>();

        List<T> comps = new List<T>(root.GetComponentsInChildren<T>(true));
        return comps.Find(d => d.name.Equals(str) == true);
    }

    public static T GetComponent<T>(this Component root, string str) where T : Component
    {
        if (root.gameObject.name.Equals(str))
            return root.GetComponent<T>();

        List<T> comps = new List<T>(root.GetComponentsInChildren<T>(true));
        return comps.Find(d => d.name.Equals(str) == true);
    }

    public static List<T> GetComponents<T>(this GameObject root, string str) where T : Component
    {
        List<T> comps = new List<T>(root.GetComponentsInChildren<T>(true));
        return comps.FindAll(d => d.name.Equals(str) == true);
    }

    public static List<T> GetComponents<T>(this Component root, string str) where T : Component
    {
        List<T> comps = new List<T>(root.GetComponentsInChildren<T>(true));
        return comps.FindAll(d => d.name.Equals(str) == true);
    }

    /// <summary>
    /// Distance
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    public static float Distance(this Vector3 a, Vector3 b)
    {
        return Vector3.Distance(a, b);
    }

    public static float DistanceXZ(this Vector3 a, Vector3 b)
    {
        a.y = 0f;
        b.y = 0f;
        return Vector3.Distance(a, b);
    }

    private static void ApplyText(Text txt, string str)
    {
        if (null == txt)
            return;

        str = str.Replace("\\n", "\n");
        txt.text = str;

        //if (SoularkOption.Language == SystemLanguage.Japanese)
        //{            
        //    if (txt.resizeTextForBestFit == true && txt.GetComponent<ContentSizeFitter>() == null)
        //    {
        //        txt.resizeTextForBestFit = false;

        //        //// 띄어쓰기가 포함된 경우 무시
        //        //if (str.Contains("\n") == true)
        //        //    return;

        //        //if (str.Contains(" ") == true)
        //        //    return;

        //        //float width = txt.rectTransform.GetWidth();
        //        //float height = txt.rectTransform.GetHeight();

        //        //int widthTextLength = Mathf.RoundToInt(width / txt.resizeTextMaxSize);
        //        //if (widthTextLength == 0)
        //        //    return;                

        //        //string untagStr = Regex.Replace(str, "<.*?>", string.Empty);

        //        //int line = untagStr.Length / widthTextLength;
        //        //int remain = untagStr.Length % widthTextLength;

        //        //if( line < 1 )
        //        //    return;

        //        //if (line == 1 && remain < 6)
        //        //    return;

        //        //int heightLine = (int)(height / txt.resizeTextMaxSize);
        //        //if (heightLine > line + 1)
        //        //{
        //        //    txt.resizeTextForBestFit = false;
        //        //    return;
        //        //}

        //        //string temp = string.Empty;
        //        //for (int i = 0; i < line; i++)
        //        //{                    
        //        //    temp += untagStr.Substring(i * widthTextLength, widthTextLength);
        //        //    if (i < line - 1)
        //        //        temp += "\n";
        //        //}

        //        //if (remain > 0)
        //        //{
        //        //    if (remain > 5)
        //        //        temp += "\n";

        //        //    temp += untagStr.Substring(line * widthTextLength, remain);
        //        //    txt.text = temp.Replace("\\n", "\n"); ;
        //        //}
        //    }
        //}
    }

    public static void SetText(this Text txt, string str)
    {
        if (null == txt)
        {
            return;
        }

        if (string.IsNullOrEmpty(str))
        {
            txt.text = "";
            return;
        }

        ApplyText(txt, str);
    }

    public static void SetText(this Text txt, string format, params object[] args)
    {
        if (null == txt)
        {
            return;
        }

        if (string.IsNullOrEmpty(format))
        {
            txt.text = string.Empty;
        }
        else
        {
            string str = string.Format(format, args);
            ApplyText(txt, str);
        }
    }
    
    /// <summary>
    /// 천 단위 표시
    /// </summary>
    public static void SetUnitText(this Text txt, int amount)
    {
        txt.text = string.Format("{0:#,##0}", amount);
    }

    public static void SetUnitText(this Text txt, long amount)
    {
        txt.text = string.Format("{0:#,##0}", amount);
    }

    public static void SetPersentTex(this Text txt, float amount)
    {
        txt.text = string.Format("{0:F2}%", amount);
    }

    public static bool IsDestroyed(this GameObject gameObject)
    {
        // UnityEngine overloads the == opeator for the GameObject type
        // and returns null when the object has been destroyed, but 
        // actually the object is still there but has not been cleaned up yet
        // if we test both we can determine if the object has been destroyed.
        return gameObject == null && !ReferenceEquals(gameObject, null);
    }

    public static bool IsNull<T, TU>(this KeyValuePair<T, TU> pair)
    {
        return pair.Equals(new KeyValuePair<T, TU>());
    }

    public static Color32 HexToColor(this string hex)
    {
        if (string.IsNullOrEmpty(hex))
            return new Color32(255, 255, 255, 255);

        hex = hex.Replace("#", "");
        byte a = 255;
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);

        if (hex.Length == 8)
            a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);

        return new Color32(r, g, b, a);
    }

    public static string ColorToHex(this Color32 color)
    {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }

    public static string ColorToHex(this Color color)
    {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }
    
    public static void ChangeLayers(this GameObject go, int layer)
    {
        go.layer = layer;
        foreach (Transform child in go.transform)
        {
            ChangeLayers(child.gameObject, layer);
        }
    }
}
