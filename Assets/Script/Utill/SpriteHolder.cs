﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

namespace ProjectEgg.Resource
{
    using ProjectEgg.Util;

    public class SpriteHolder : SingletonMonobehaviour<SpriteHolder>
    {
        private List<SpriteAtlas> ListSpriteAtlas { get; set; }

        protected override void Awake()
        {
            if (_inst != null)
            {
                Destroy(this);
                return;
            }

            base.Awake();

            Initialize();
        }

        public void Initialize()
        {
            if( ListSpriteAtlas == null )
            {
                ListSpriteAtlas = new List<SpriteAtlas>();
            }

            SpriteAtlasManager.atlasRequested += RequestLateBindingAtlas;
        }

        public SpriteAtlas GetAtlas(string atlasName)
        {
            if (ListSpriteAtlas == null)
                return null;

            ListSpriteAtlas.RemoveAll(d => d == null);

            if (ListSpriteAtlas.Exists(d=>d.name == atlasName) == false)
            {
                ListSpriteAtlas.Add(Resources.Load<SpriteAtlas>(string.Format("Atlas/{0}", atlasName)));
            }

            return ListSpriteAtlas.Find(d => null != d && d.name == atlasName);
        }

        /// <summary>
        /// Get Sprite
        /// </summary>
        /// <param name="atlasName"></param>
        /// <param name="spriteName"></param>
        /// <returns></returns>
        public Sprite GetSprite( string atlasName, string spriteName )
        {
            if (string.IsNullOrEmpty(spriteName) == true)
                return null;

            SpriteAtlas atlas = GetAtlas(atlasName);
            if (null == atlas)
                return null;

            return atlas.GetSprite(spriteName);
        }


        // 아틀라스 바인딩 콜백함수
        void RequestLateBindingAtlas(string atlasName, System.Action<SpriteAtlas> action)
        {
            Debug.Log("RequestLateBindingAtlas Tag: " + atlasName);
            if (null != action)
            {
                action.Invoke(GetAtlas(atlasName));
            }
        }

        public void ClearCache()
        {
            foreach(var asset in ListSpriteAtlas)
            {
                if (null == asset)
                    continue;

                Resources.UnloadAsset(asset);
                System.GC.SuppressFinalize(asset);
            }

            ListSpriteAtlas.Clear();
        }

        public void ClearAtlas( string atlasName )
        {
            var asset = ListSpriteAtlas.Find(d => null != d && d.name == atlasName);

            if (null == asset)
                return;

            ListSpriteAtlas.Remove(asset);
            Resources.UnloadAsset(asset);
            System.GC.SuppressFinalize(asset);
            asset = null;
        }

        public void Dispose()
        {
            ListSpriteAtlas.Clear();
            SpriteAtlasManager.atlasRequested -= RequestLateBindingAtlas;
        }

        /// <summary>
        /// 리소스 가져오기
        /// </summary>
        /// <param name="prefab"></param>
        /// <returns></returns>
        public Sprite GetCharacter(string prefab)
        {
            return GetSprite("CharacterProb", prefab);
        }
    }
}
