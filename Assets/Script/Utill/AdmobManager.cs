﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Test IDS
/// </summary>
/*
 *  배너 광고	ca-app-pub-3940256099942544/6300978111
    전면 광고	ca-app-pub-3940256099942544/1033173712
    전면 동영상 광고	ca-app-pub-3940256099942544/8691691433
    보상형 동영상 광고	ca-app-pub-3940256099942544/5224354917
    네이티브 광고 고급형	ca-app-pub-3940256099942544/2247696110
    네이티브 동영상 광고 고급형	ca-app-pub-3940256099942544/1044960115
 * */

namespace ProjectEgg
{
    using GoogleMobileAds.Api;
    using ProjectEgg.Game;
    using ProjectEgg.Util;
    using System;

    public class AdmobManager : SingletonMonobehaviour<AdmobManager>
    {
        // Google Ads Mob App ID = ca-app-pub-4309191501423793~4144469119

        /// <summary>
        /// 전면광고 닫기 리턴 함수
        /// </summary>
        private Action _resScreenAdClose;

        /// <summary>
        /// 보상형 광고 리턴 결과
        /// </summary>
        private Action<string> _resRewardAd;

        bool IsTestMode = true;

        private bool _isInitialize = false;
        public void Initialize()
        {
            if (true == _isInitialize)
                return;

            Debug.Log("AdMob Manager Initialize");
            _isInitialize = true;

            MobileAds.Initialize((initstatus) =>
            {
                _isInitialize = true;

                // 광고제거 상품을 구매한 경우 광고를 사용하지 않음
                if (false == DataObject.Instance.IsBuyDeleteAds())
                {
                    Init_lineAds();
                    Init_FullAds();
                }

                Init_HintAds();
            });
        }

        AdRequest CreateAdRequest()
        {
            var adRequest = new AdRequest.Builder();
            if(IsTestMode)
            {
                adRequest.AddTestDevice("A1650AF62F79A9809B1D6B9C1189D6AB"); // KJW : 회사 LD Player
            }
            return adRequest.Build();
        }

        /// <summary>
        /// 베너 광고
        /// </summary>
        #region

        private BannerView _banner;
        private void Init_lineAds()
        {
            // Production LineBanner - ID
            string unitID = "ca-app-pub-4309191501423793/6118868099";

            // Develop LineBanner - ID
            string test_unitID = "ca-app-pub-3940256099942544/6300978111";

            string id = IsTestMode ? test_unitID : unitID;

            if (_banner != null)
            {
                _banner.Destroy();
            }

            _banner = new BannerView(id, AdSize.SmartBanner, AdPosition.Bottom);

            AdRequest request = CreateAdRequest();
            _banner.LoadAd(request);
        }

        #endregion

        /// <summary>
        /// 전면 광고
        /// </summary>
        #region

        private InterstitialAd _screenAd;
        private bool _isloading_screenAd = false;
        private void Init_FullAds()
        {
            // Production ScreenBanner - ID
            string unitID = "ca-app-pub-4309191501423793/1529435130";

            // Develop ScreenBanner - ID
            string test_unitID = "ca-app-pub-3940256099942544/1033173712";

            string id = IsTestMode ? test_unitID : unitID;

            if (null != _screenAd)
            {
                _screenAd.Destroy();
            }

            _screenAd = new InterstitialAd(id);

            AdRequest request = CreateAdRequest();
            
            _screenAd.OnAdClosed += _screenAd_OnAdClosed;
            _screenAd.OnAdFailedToLoad += _screenAd_OnAdFailedToLoad;
            _screenAd.OnAdLeavingApplication += _screenAd_OnAdLeavingApplication;
            _screenAd.OnPaidEvent += _screenAd_OnPaidEvent;
            _screenAd.OnAdLoaded += _screenAd_OnAdLoaded;
            _screenAd.OnAdOpening += _screenAd_OnAdOpening;

            _screenAd.LoadAd(request);
        }

        private void _screenAd_OnAdOpening(object sender, EventArgs e)
        {
            Debug.Log("_screenAd_OnAdOpening");
        }

        private void _screenAd_OnAdLoaded(object sender, EventArgs e)
        {
            Debug.Log("_screenAd_OnAdLoaded");
        }

        private void _screenAd_OnPaidEvent(object sender, AdValueEventArgs e)
        {
            Debug.Log("_screenAd_OnPaidEvent");
            _isloading_screenAd = false;
            
        }

        private void _screenAd_OnAdLeavingApplication(object sender, EventArgs e)
        {
            Debug.Log("_screenAd_OnAdLeavingApplication");
            _isloading_screenAd = false;
        }

        private void _screenAd_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
        {
            Debug.Log("_screenAd_OnAdFailedToLoad");
            _isloading_screenAd = false;
        }

        private void _screenAd_OnAdClosed(object sender, EventArgs e)
        {
            Debug.Log("_screenAd_OnAdClosed");
            //_resScreenAdClose?.Invoke();
            _isloading_screenAd = false;
        }

        /// <summary>
        /// 전면광고 오픈
        /// </summary>
        public void ShowScreenAdBanner(Action res)
        {
            if (DataObject.Instance.IsBuyDeleteAds())
                return;
            if (true == _isloading_screenAd)
                return;

            _resScreenAdClose = res;
            StartCoroutine(ShowScreenAd());
        }

        /// <summary>
        /// 전면광고 오픈 시작
        /// </summary>
        /// <returns></returns>
        private IEnumerator ShowScreenAd()
        {
            _isloading_screenAd = true;

#if UNITY_EDITOR
            _isloading_screenAd = false;
            _resScreenAdClose();
            
            yield return null;
#else
            Debug.Log("Start Load Ad - Screen");
            while (!_screenAd.IsLoaded())
                yield return null;

            _screenAd.Show();
#endif


        }

        #endregion

        /// <summary>
        /// 보상형 광고 ( 힌트 )
        /// </summary>
        #region

        private bool _isloading_rewardAd = false;
        private RewardedAd _rewardedAd;
        private void Init_HintAds()
        {
            // Production LineBanner - ID
            string unitID = "ca-app-pub-4309191501423793/1337863447";

            // Develop LineBanner - ID
            string test_unitID = "ca-app-pub-3940256099942544/5224354917"; // ca-app-pub-4309191501423793/1337863447

            string id = IsTestMode ? test_unitID : unitID;

            _rewardedAd = new RewardedAd(id);

            AdRequest request = CreateAdRequest();
            
            _rewardedAd.OnAdFailedToLoad += _rewardedAd_OnAdFailedToLoad;
            _rewardedAd.OnAdFailedToShow += _rewardedAd_OnAdFailedToShow;
            _rewardedAd.OnPaidEvent += _rewardedAd_OnPaidEvent;
            _rewardedAd.OnUserEarnedReward += _rewardedAd_OnUserEarnedReward;
            _rewardedAd.OnAdClosed += _rewardedAd_OnAdClosed;

            _rewardedAd.LoadAd(request);
        }

        /// <summary>
        /// 광고가 종료될경우 무조건 들어옴
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _rewardedAd_OnAdClosed(object sender, EventArgs e)
        {
            _isloading_rewardAd = false;
            _resRewardAd("OnAdClosed");
        }

        /// <summary>
        /// 광고를 끝까지 보고나서 종료할 경우 들어옴
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _rewardedAd_OnUserEarnedReward(object sender, Reward e)
        {
            _isloading_rewardAd = false;
            _resRewardAd("OnUserEarnedReward");
        }

        private void _rewardedAd_OnPaidEvent(object sender, AdValueEventArgs e)
        {
            _isloading_rewardAd = false;
            _resRewardAd("OnPaidEvent");
        }

        private void _rewardedAd_OnAdFailedToShow(object sender, AdErrorEventArgs e)
        {
            _isloading_rewardAd = false;
            _resRewardAd("OnAdFailedToShow");
        }

        private void _rewardedAd_OnAdFailedToLoad(object sender, AdErrorEventArgs e)
        {
            _isloading_rewardAd = false;
            _resRewardAd("OnAdFailedToLoad");
        }

        /// <summary>
        /// 보상형 광고 오픈
        /// </summary>
        public void ShowRewardMovieAd(Action<string> res)
        {
            if (DataObject.Instance.IsBuyDeleteAds())
                return;
            if (true == _isloading_rewardAd)
                return;

            _resRewardAd = res;
            StartCoroutine(ShowRewardAd());
        }

        /// <summary>
        /// 보상형 광고 오픈 시작
        /// </summary>
        /// <returns></returns>
        private IEnumerator ShowRewardAd()
        {
            _isloading_rewardAd = true;

#if UNITY_EDITOR
            _isloading_rewardAd = false;
            _resRewardAd("");
            yield return null;
#else
            Debug.Log("Start Load Ad - Movie");
            while (!_rewardedAd.IsLoaded())
                yield return null;

            _rewardedAd.Show();
#endif
        }

        #endregion
        public void DestroyAd()
        {
        }
    }
}

