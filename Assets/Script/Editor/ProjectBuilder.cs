﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

using ProjectEgg.Util;
public class ProjectBuilder
{
    private static string[] SCENES = FindEnabledEditorScenes();

    /// <summary>
    /// Find Scenes for APK Build
    /// </summary>
    /// <returns></returns>
    private static string[] FindEnabledEditorScenes()
    {
        List<string> editorScenes = new List<string>();

        foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
        {
            if (!scene.enabled)
                continue;

            editorScenes.Add(scene.path);
        }

        return editorScenes.ToArray();
    }

    /// <summary>
    /// Production AOS Build
    /// </summary>
    public static void AndroidBuild(string targerDir, string packageName, string keystoreName , string keystorePassword, string keyAlias, string keyAlisePassword, bool isDevelopmentBuild)
    {
        PlayerPrefs.SetString(ProjectEggDefine.KEY_STORE_NAME, keystoreName);
        PlayerPrefs.SetString(ProjectEggDefine.KEY_STORE_PASSWORD, keystorePassword);
        PlayerPrefs.SetString(ProjectEggDefine.KEY_ALIAS, keyAlias);
        PlayerPrefs.SetString(ProjectEggDefine.KEY_ALIAS_PASSWORD, keyAlisePassword);
        PlayerPrefs.SetString(ProjectEggDefine.PACKAGE_NAME, packageName);
        PlayerPrefs.SetInt(ProjectEggDefine.DEVELOPMENT_BUILD, isDevelopmentBuild == true ? 1 : 0);

        char sep = Path.DirectorySeparatorChar;

        DateTime newTime = DateTime.Now;
        string time = string.Format("Y{0}-M{1}-D{2}_h{3}_m{4}", newTime.Year, newTime.Month, newTime.Day, newTime.Hour, newTime.Minute);
        string buildPath = $"{targerDir}{sep}AOS_v{PlayerSettings.bundleVersion}_b{PlayerSettings.Android.bundleVersionCode}_{time}.apk";

        PlayerSettings.Android.keystoreName = keystoreName;
        PlayerSettings.Android.keystorePass = keystorePassword;
        PlayerSettings.Android.keyaliasName = keyAlias;
        PlayerSettings.Android.keyaliasPass = keyAlisePassword;
        
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, packageName);

        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
        
        BuildReport buildres;
        if(isDevelopmentBuild)
            buildres = BuildPipeline.BuildPlayer(SCENES, buildPath, BuildTarget.Android, BuildOptions.Development);
        else
            buildres = BuildPipeline.BuildPlayer(SCENES, buildPath, BuildTarget.Android, BuildOptions.None);

        BuildSummary summary = buildres.summary;
        if (summary.result == BuildResult.Succeeded)
        {
            EditorUtility.DisplayDialog("Info", "Build Finished!", "OK");
            EditorUtility.RevealInFinder(buildPath);
        }
    }
}

public class ProjectBuilderWindow : EditorWindow
{
    private static string _buildPath = string.Empty;
    private static string _screenShotPath = string.Empty;

    private static string keystoreName;
    private static string keystorePassword;
    private static string keyAlias;
    private static string keyAliasPassword;
    private static string packegeName;
    private static bool isDevelopmentBuild;

#if !UNITY_IOS
    [MenuItem("CI/Android Build Window")]
    private static void Init()
    {
        ProjectBuilderWindow window = (ProjectBuilderWindow)EditorWindow.GetWindow(typeof(ProjectBuilderWindow), false, "Android Build Window");
        window.minSize = new Vector2(370, 350);
        window.Show();
    }
#endif

    Rect window_listRect = new Rect(10, 10, 270, 300);
    private void OnEnable()
    {
        Load();
    }
    private void OnGUI()
    {
        // 상단 버튼 3종
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Load", GUILayout.Width(100f)))
            {
                Load();
            }
            if (GUILayout.Button("Save", GUILayout.Width(100f)))
            {
                Save();
            }
            if (GUILayout.Button("Get Default & Save", GUILayout.Width(150f)))
            {
                GetDefault();
            }
            GUILayout.EndHorizontal();
        }
        
        GUILayout.Space(5);

        // Key Store Info Text
        {
            GUILayout.BeginHorizontal("PopupCurveSwatchBackground");
            GUILayout.Space(7);
            if (GUILayout.Button("<< Key Store Info >>", "ChannelStripAttenuationBar"))
            {
                EditorGUIUtility.systemCopyBuffer = "\"" + "ChannelStripAttenuationBar" + "\"";
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }
        
        GUILayout.Space(5);

        // Build Options
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Package Name :", GUILayout.Width(130));
            packegeName = EditorGUILayout.TextField(packegeName, GUILayout.Width(200));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("keystore Pass :", GUILayout.Width(130));
            keystoreName = EditorGUILayout.TextField(keystoreName, GUILayout.Width(200));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("keystore Password :", GUILayout.Width(130));
            keystorePassword = EditorGUILayout.TextField(keystorePassword, GUILayout.Width(200));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("key Alias :", GUILayout.Width(130));
            keyAlias = EditorGUILayout.TextField(keyAlias, GUILayout.Width(200));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("key Alias Password :", GUILayout.Width(130));
            keyAliasPassword = EditorGUILayout.TextField(keyAliasPassword, GUILayout.Width(200));
            EditorGUILayout.EndHorizontal();

            isDevelopmentBuild = EditorGUILayout.Toggle("Is Development Build", isDevelopmentBuild);
        }

        GUILayout.Space(20);

        // Screen Shot Text
        {
            GUILayout.BeginHorizontal("PopupCurveSwatchBackground");
            GUILayout.Space(7);
            if (GUILayout.Button("<< ScreenShot Path >>", "ChannelStripAttenuationBar"))
            {
                EditorGUIUtility.systemCopyBuffer = "\"" + "ChannelStripAttenuationBar" + "\"";
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(5);

        // Screen Show Path & Button
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button(". . .", GUILayout.Width(30f)))
            {
                var path = EditorUtility.OpenFolderPanel("Set ScreenShot Path...", string.IsNullOrEmpty(_buildPath) ? Path.GetFullPath(".") : _screenShotPath, string.Empty);
                if (Directory.Exists(path) ||
                    path != _screenShotPath)
                {
                    EditorPrefs.SetString(ProjectEggDefine.SCREENSHOT_PATH, path);
                    _screenShotPath = path;
                }
            }
            GUILayout.Label($" : {_screenShotPath}");
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Screen Capcher"))
            {
                if (string.IsNullOrEmpty(_screenShotPath))
                {
                    EditorUtility.DisplayDialog("Screen Capcher", "Screen Shot 을 저장할 폴더를 지정해주세요 !", "OK");
                    return;
                }
                else
                {
                    ScreenCapture.CaptureScreenshot($"{_screenShotPath}{Path.DirectorySeparatorChar}{DateTime.Now.ToShortDateString()}_{DateTime.Now.Hour}-{DateTime.Now.Minute}-{DateTime.Now.Second + DateTime.Now.Millisecond}.png");
                    Debug.Log("Screen Capcher !!");
                }
            }
        }
        
        GUILayout.Space(20);

        // Build Path Text
        {
            GUILayout.BeginHorizontal("PopupCurveSwatchBackground");
            GUILayout.Space(7);
            if (GUILayout.Button("<< Build Path >>", "ChannelStripAttenuationBar"))
            {
                EditorGUIUtility.systemCopyBuffer = "\"" + "ChannelStripAttenuationBar" + "\"";
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }
        
        GUILayout.Space(5);

        // Build Path & Button
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button(". . .", GUILayout.Width(30f)))
            {
                var path = EditorUtility.OpenFolderPanel("Set Build Path...", string.IsNullOrEmpty(_buildPath) ? Path.GetFullPath(".") : _buildPath, string.Empty);
                if (Directory.Exists(path) ||
                    path != _buildPath)
                {
                    EditorPrefs.SetString(ProjectEggDefine.BUILD_PATH, path);
                    _buildPath = path;
                    Debug.Log("_buildPath : " + _buildPath);
                }
            }
            GUILayout.Label($" : {_buildPath}");
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Android Build Now!"))
            {
                if (string.IsNullOrEmpty(_buildPath))
                {
                    EditorUtility.DisplayDialog("Build", "Build Path를 지정해주세요!", "OK");
                    return;
                }

                if (EditorUtility.DisplayDialog("Build", "빌드하시겠습니까?", "Build", "Cancel"))
                {
                    ProjectBuilder.AndroidBuild(_buildPath, packegeName, keystoreName, keystorePassword, keyAlias, keyAliasPassword, isDevelopmentBuild);
                }
            }
        }
        

        //Example();
    }

    /// <summary>
    /// 데이터 가져오기
    /// </summary>
    private void Load()
    {
        _buildPath = EditorPrefs.GetString(ProjectEggDefine.BUILD_PATH);
        _screenShotPath = EditorPrefs.GetString(ProjectEggDefine.SCREENSHOT_PATH);
        keystoreName = PlayerPrefs.GetString(ProjectEggDefine.KEY_STORE_NAME);
        keystorePassword = PlayerPrefs.GetString(ProjectEggDefine.KEY_STORE_PASSWORD);
        keyAlias = PlayerPrefs.GetString(ProjectEggDefine.KEY_ALIAS);
        keyAliasPassword = PlayerPrefs.GetString(ProjectEggDefine.KEY_ALIAS_PASSWORD);
        packegeName = PlayerPrefs.GetString(ProjectEggDefine.PACKAGE_NAME);
        isDevelopmentBuild = PlayerPrefs.GetInt(ProjectEggDefine.DEVELOPMENT_BUILD) == 1 ? true : false;
    }

    /// <summary>
    /// 데이터 저장하기
    /// </summary>
    private void Save()
    {
        EditorPrefs.SetString(ProjectEggDefine.BUILD_PATH, _buildPath);
        EditorPrefs.SetString(ProjectEggDefine.SCREENSHOT_PATH, _screenShotPath);
        PlayerPrefs.SetString(ProjectEggDefine.KEY_STORE_NAME, keystoreName);
        PlayerPrefs.SetString(ProjectEggDefine.KEY_STORE_PASSWORD, keystorePassword);
        PlayerPrefs.SetString(ProjectEggDefine.KEY_ALIAS, keyAlias);
        PlayerPrefs.SetString(ProjectEggDefine.KEY_ALIAS_PASSWORD, keyAliasPassword);
        PlayerPrefs.SetString(ProjectEggDefine.PACKAGE_NAME, packegeName);
        PlayerPrefs.SetInt(ProjectEggDefine.DEVELOPMENT_BUILD, isDevelopmentBuild == true ? 1 : 0);
    }

    /// <summary>
    /// 기본값 가져오기
    /// </summary>
    private void GetDefault()
    {
        PlayerPrefs.SetString(ProjectEggDefine.KEY_STORE_NAME, "KeyStore/EggPaint.keystore");
        PlayerPrefs.SetString(ProjectEggDefine.KEY_STORE_PASSWORD, "funnytown");
        PlayerPrefs.SetString(ProjectEggDefine.KEY_ALIAS, "eggpaint");
        PlayerPrefs.SetString(ProjectEggDefine.KEY_ALIAS_PASSWORD, "funnytown");
        PlayerPrefs.SetString(ProjectEggDefine.PACKAGE_NAME, "com.FunnyTown.EggPaint");
        PlayerPrefs.SetInt(ProjectEggDefine.DEVELOPMENT_BUILD, 1);
        Load();
    }

    //Vector2 scrollPosition = new Vector2(0, 0);
    //string search = "";
    //private void Example()
    //{
    //    GUILayout.BeginHorizontal("HelpBox");
    //    GUILayout.Label("Click a Sample to copy its Name to your Clipboard", "MiniBoldLabel");
    //    GUILayout.FlexibleSpace();
    //    GUILayout.Label("Search:");
    //    search = EditorGUILayout.TextField(search);

    //    GUILayout.EndHorizontal();
    //    scrollPosition = GUILayout.BeginScrollView(scrollPosition);

    //    foreach (var style in GUI.skin.customStyles)
    //    {
    //        if (style.name.ToLower().Contains(search.ToLower()))
    //        {
    //            GUILayout.BeginHorizontal("PopupCurveSwatchBackground");
    //            GUILayout.Space(7);
    //            if (GUILayout.Button(style.name, style))
    //            {
    //                EditorGUIUtility.systemCopyBuffer = "\"" + style.name + "\"";
    //            }
    //            GUILayout.FlexibleSpace();
    //            EditorGUILayout.SelectableLabel("\"" + style.name + "\"");
    //            GUILayout.EndHorizontal();
    //            GUILayout.Space(11);
    //        }
    //    }

    //    GUILayout.EndScrollView();
    //}
}