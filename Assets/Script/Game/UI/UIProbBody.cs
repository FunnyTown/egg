﻿using ProjectEgg.Resource;
using ProjectEgg.Util;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using ProjectEgg.TableData;
using System;

namespace ProjectEgg.Game
{
    public class UIProbBody : MonoBehaviour
    {
        /// <summary>
        /// 데이터
        /// </summary>
        private TB_Prob _tbProb { get; set; }

        /// <summary>
        /// 몸통 이미지
        /// </summary>
        private Image _image_BodyImage;

        public int TBProbRefId { get; set; }
        public float Size_Width { get; set; }
        public float Size_Height { get; set; }
        public int ColorPos_x { get; set; }
        public int ColorPos_y { get; set; }
        public string ImageName { get; set; }
        public Type_Prob ProbType { get; set; }
        
        private bool _isFindUI = false;
        private void FindUI()
        {
            if (true == _isFindUI)
                return;

            _image_BodyImage = GetComponent<Image>();
            _image_BodyImage.color = Color.black;

            _isFindUI = true;
        }

        /// <summary>
        /// 생성
        /// </summary>
        public void Initialize(int ProbRefId)
        {
            _tbProb = TBData.Instance.TB_Prob.Find(d => d.ID == ProbRefId);
            if(null == _tbProb)
            {
                gameObject.SetActive(false);
                return;
            }

            FindUI();
            gameObject.SetActive(true);
            TBProbRefId = ProbRefId;

            // ProbImage
            _image_BodyImage.sprite = SpriteHolder.Instance.GetCharacter(_tbProb.ImageName);
            _image_BodyImage.SetNativeSize();
            _image_BodyImage.enabled = false;

            // Position
            float posx = _tbProb.PositionX - _image_BodyImage.rectTransform.GetWidth() * 0.5f;
            float posy = _tbProb.PositionY - _image_BodyImage.rectTransform.GetHeight() * 0.5f;
            Vector3 localPosition = new Vector3(posx, posy, 0);
            gameObject.transform.localPosition = localPosition;

            // Name
            gameObject.name = _tbProb.Type_Prob.ToString();

            // LocalData
            var rectTransform = gameObject.GetComponent<RectTransform>();
            Size_Width = _image_BodyImage.rectTransform.GetWidth();
            Size_Height = _image_BodyImage.rectTransform.GetHeight();
            ColorPos_x = (int)rectTransform.anchoredPosition.x;
            ColorPos_y = (int)rectTransform.anchoredPosition.y;
            ProbType = _tbProb.Type_Prob;
            ImageName = _tbProb.ImageName;
        }
        
        /// <summary>
        /// 선택중인 바디 갯수가 변경됨
        /// </summary>
        public void SelectedBodyChange()
        {
            // 'Body' Prob 은 제외
            if (_tbProb.Type_Prob == Type_Prob.Body)
                return;

            // 선택중인 Prob List 에 '내' 가 있는경우
            // TODO jaewoo
            // - tween 에서 이동하고 있으므로 우선은 없엠.
            //_image_BodyImage.enabled = DataObject.Instance.SelectedBodys.Contains(_tBDataProb.TypeProb);
        }
    }
}