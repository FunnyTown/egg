﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

public abstract class ProjectEggBaseWindow : MonoBehaviour
{
    private CanvasGroup _canvasGroup = null;

    public UnityAction OpenEvent = null;
    private bool isOpenEnd = false;

    protected virtual void OnEnable()
    {
        if (null == _canvasGroup)
            _canvasGroup = gameObject.GetComponent<CanvasGroup>();

        _canvasGroup.alpha = 0;
        IDisposable dispose = null;
        dispose = Observable.IntervalFrame(1).Subscribe(_ =>
        {
            if (_canvasGroup.alpha < 1)
                _canvasGroup.alpha += 0.05f;

            if (_canvasGroup.alpha >= 1)
            {
                dispose.Dispose();
                dispose = null;

                // 데이터 셋팅
                if (true == isOpenEnd)
                    return;

                if (null != OpenEvent)
                {
                    isOpenEnd = true;
                    OpenEvent.Invoke();
                }
                    
            }
        }).AddTo(this);
    }

    protected abstract void FindUI();

    public virtual void OnClose()
    {
        ProjectEggUIManager.Instance.CloseWindow(gameObject.name);
    }
}
