﻿using ProjectEgg.Game;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.UI;

namespace ProjectEgg.EditorMode
{
    /// <summary>
    /// 엔진일 경우에만 사용하는 EditorUI
    /// </summary>
    public class UIEditorMode : MonoBehaviour
    {
        public UIGame GameParent;

        private Text _text_ClickCount;

        /// <summary>
        /// 클릭 카운트 감지
        /// </summary>
        private IDisposable _dispose;

        private bool _isFindUI = false;
        private void FindUI()
        {
            if (true == _isFindUI)
                return;
            
            var findButton = gameObject.GetComponent<Button>("Button_Reset");
            findButton.onClick.AddListener(OnClick_Reset);

            findButton = gameObject.GetComponent<Button>("Button_Play");
            findButton.onClick.AddListener(OnClick_Help);
            
            findButton = gameObject.GetComponent<Button>("Button_Log");
            findButton.onClick.AddListener(OnClick_Log);

            _text_ClickCount = gameObject.GetComponent<Text>("Text_Click");

            if (null != _dispose)
            {
                _dispose.Dispose();
                _dispose = null;
            }

            // 프랍 아이템 선택 감지
            _dispose = EditorDataObject.Instance.ClickLog.ObserveCountChanged().Subscribe(_ =>
            {
                _text_ClickCount.SetText(EditorDataObject.Instance.ClickLog.Count.ToString());
            }).AddTo(this);

            _isFindUI = true;
        }

        private void OnDisable()
        {
            EditorDataObject.Instance.IsEditorMode.Value = false;
            if (null != _dispose)
            {
                _dispose.Dispose();
                _dispose = null;
            }
        }

        public void Initialize()
        {
            FindUI();
        }

        /// <summary>
        /// 초기화
        /// </summary>
        public void OnClick_Reset()
        {
            if (DataObject.Instance.IsPlayHelp)
                return;
            
            EditorDataObject.Instance.ClickLog.Clear();
            GameParent.GameClear();
        }

        /// <summary>
        /// Help 출력
        /// </summary>
        public void OnClick_Help()
        {
            if (DataObject.Instance.IsPlayHelp)
                return;

            GameParent.GameClear();
            GameParent.ShowHelp();
        }

        /// <summary>
        /// 클릭 로그 보기
        /// </summary>
        public void OnClick_Log()
        {
            Debug.Log(string.Format("=========== {0} Log ===========" , DateTime.Now.ToString()));

            string log = string.Empty;
            var collection = EditorDataObject.Instance.ClickLog;
            foreach(string item in collection)
            {
                log = string.Format("{0}{1}", log, item);
            }

            if(0 < log.Length)
                log = log.Remove(0, 1);
            Debug.Log(log);

            Debug.Log("===============================");
        }

        private void OnEnable()
        {
            EditorDataObject.Instance.IsEditorMode.Value = true;
        }
    }
}