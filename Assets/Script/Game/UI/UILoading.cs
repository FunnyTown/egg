﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectEgg.Lobby
{
    public class UILoading : ProjectEggBaseWindow
    {
        /// <summary>
        /// 로딩상태 텍스트 
        /// </summary>
        private IDisposable _dispose { get; set; }

        /// <summary>
        /// 텍스트
        /// </summary>
        private Text _text_Loading { get; set; }

        /// <summary>
        /// 구글 로그인 버튼
        /// </summary>
        private Button _button_GoogleLogin { get; set; }

        /// <summary>
        /// 구글 로그아웃 버튼
        /// </summary>
        private Button _button_GoogleLogOut { get; set; }

        private Button _button_Start { get; set; }

        protected override void OnEnable()
        {
            OpenEvent = UIOpenEnd;
            base.OnEnable();
        }

        private bool _isFindUI = false;
        protected override void FindUI()
        {
            if (true == _isFindUI)
                return;

            _text_Loading = gameObject.GetComponent<Text>("Text_Loading");

            _button_GoogleLogin = gameObject.GetComponent<Button>("Button_Google");
            _button_GoogleLogin.onClick.AddListener(LoginStart);

            _button_GoogleLogOut = gameObject.GetComponent<Button>("Button_GoogleLogout");
            _button_GoogleLogOut.onClick.AddListener(() =>
            {
                ProjectEggLoginManager.Instance.OnLogOut();
                _button_GoogleLogin.interactable = true;
                _button_GoogleLogOut.interactable = false;
            });

            _button_Start = gameObject.GetComponent<Button>("Button_Start");
            _button_Start.onClick.AddListener(OnClick_GameStart);

            _isFindUI = true;
        }

        private void UIOpenEnd()
        {
            FindUI();

#if UNITY_EDITOR
            _button_GoogleLogin.interactable = false;
            _button_GoogleLogOut.interactable = false;
            _button_Start.interactable = true;
#else
            _button_GoogleLogin.interactable = !Social.localUser.authenticated;
            _button_GoogleLogOut.interactable = Social.localUser.authenticated;
            _button_Start.interactable = Social.localUser.authenticated;
            AdmobManager.Instance.Initialize();
#endif

        }

        /// <summary>
        /// 로그인 시작
        /// </summary>
        private void LoginStart()
        {
            if (null != _dispose)
            {
                _dispose.Dispose();
                _dispose = null;
            }

            _dispose = ProjectEggLoginManager.Instance.LoginState.ObserveEveryValueChanged(v => v).Subscribe((state) =>
            {
                _text_Loading.text = ProjectEggLoginManager.Instance.LoginState.Value;
            }).AddTo(this);

            ProjectEggLoginManager.Instance.OnLogin_Auto_Google((isSuccess) =>
            {
                _button_GoogleLogin.interactable = false;

                if (true == isSuccess)
                {
                    _button_GoogleLogin.interactable = !Social.localUser.authenticated;
                    _button_GoogleLogOut.interactable = Social.localUser.authenticated;
                    _button_Start.interactable = Social.localUser.authenticated;
                }
            });
        }

        private void OnDisable()
        {
            if(null != _dispose)
            {
                _dispose.Dispose();
                _dispose = null;
            }
        }

        public void OnClick_GameStart()
        {
            //AdmobManager.Instance.ShowScreenAdBanner(null);
            ProjectEggUIManager.Instance.OpenWindow("UIGame");
            base.OnClose();
        }
    }
}