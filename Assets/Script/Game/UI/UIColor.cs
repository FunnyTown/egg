﻿using UnityEngine;
using UnityEngine.UI;

namespace ProjectEgg.Game
{
    public class UIColor : MonoBehaviour
    {
        private string _colorString { get; set; }

        private Image _image_Color;

        private int _colorIndex;

        private bool _isfindUI = false;
        private void FindUI()
        {
            if (true == _isfindUI)
                return;

            _image_Color = gameObject.GetComponent<Image>("Image_Color");
            var findBtn = gameObject.GetComponent<Button>();
            findBtn.onClick.AddListener(OnClick_Color);

            _isfindUI = true;
        }

        public void Initialize(string ColorString, int colorIndex)
        {
            _colorString = ColorString;
            if(string.IsNullOrEmpty(_colorString))
            {
                gameObject.SetActive(false);
                return;
            }

            // 컬러 순서 번호
            _colorIndex = colorIndex;

            FindUI();
            gameObject.SetActive(true);

            _image_Color.color = _colorString.HexToColor();
        }

        /// <summary>
        /// Color 선택
        /// </summary>
        public void OnClick_Color()
        {
            if (DataObject.Instance.IsPlayHelp)
                return;

            DataObject.Instance.LastSelectColor.Value = _colorString;

            if (EditorDataObject.Instance.IsEditorMode.Value)
                EditorDataObject.Instance.ClickLog.Add(string.Format(";C{0}", _colorIndex));
        }
    }
}