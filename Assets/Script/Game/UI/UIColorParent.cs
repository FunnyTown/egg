﻿namespace ProjectEgg.Game
{
    using ProjectEgg.TableData;
    using System.Collections.Generic;
    using UniLinq;
    using UnityEngine;

    public class UIColorParent : MonoBehaviour
    {
        /// <summary>
        /// 페인트 컬러통 갯수
        /// </summary>
        private const int COLOR_PAINT_COUNT = 4;

        /// <summary>
        /// Prob Item
        /// </summary>
        private List<UIColor> _colors { get; set; }

        private bool _isFindUI = false;
        /// <summary>
        /// UI 검색
        /// </summary>
        private void FindUI()
        {
            if (true == _isFindUI)
                return;

            _colors = new List<UIColor>();
            for(int i = 0; i < COLOR_PAINT_COUNT; i ++)
            {
                var findItem = gameObject.GetComponent<UIColor>(string.Format("ColorItem_{0}", i + 1));
                if(null != findItem)
                    _colors.Add(findItem);
            }
            
            _isFindUI = true;
        }

        /// <summary>
        /// 컬러 데이터 셋팅 시작
        /// </summary>
        public void Initialize()
        {
            if (null == _colors)
                _colors = new List<UIColor>();

            FindUI();
            foreach (var slot in _colors)
                slot.gameObject.SetActive(false);

            int stageIndex = DataObject.Instance.StageRefId.Value;
            TB_Stage tbStage = TBData.Instance.TB_Stage.FirstOrDefault(d => d.ID == stageIndex);
            if (null == tbStage)
                return;

            TB_Color tbColor = TBData.Instance.TB_Color.FirstOrDefault(d => d.ID == tbStage.Color_RefId);
            if (null == tbColor)
                return;

            _colors[0].Initialize(tbColor.Color1, 1);
            _colors[1].Initialize(tbColor.Color2, 2);
            _colors[2].Initialize(tbColor.Color3, 3);
            _colors[3].Initialize(tbColor.Color4, 4);
        }

        ///// <summary>
        ///// 컬러 셋팅
        ///// </summary>
        ///// <param name="ProbRefId"></param>
        //private void SetColorItem(string color, int index)
        //{
        //    UIColor findColorItem = _colors.Find(d => d.gameObject.activeSelf == false);
        //    if (null != findColorItem)
        //    {
        //        findColorItem.Initialize(color, index);
        //    }
        //    else
        //    {
        //        GameObject newItem = Instantiate(_objColorItem);
        //        newItem.transform.SetParent(transform);
        //        newItem.transform.localScale = Vector3.one;
        //        newItem.transform.SetAsLastSibling();

        //        findColorItem = newItem.GetComponent<UIColor>();
        //        _colors.Add(findColorItem);

        //        findColorItem.Initialize(color, index);
        //    }
        //}
    }
}