﻿using UnityEngine.UI;

namespace ProjectEgg.Game
{
    using ProjectEgg.TableData;
    using ProjectEgg.Util;
    using System;
    using System.Collections.Generic;
    using UniLinq;
    using UniRx;
    using UnityEngine;

    public class UIProbParent : MonoBehaviour
    {
        /// <summary>
        /// Prob Item
        /// </summary>
        private Dictionary<Type_Prob, UIProbItem> _itemsDic { get; set; }


        IDisposable _dispose;

        private bool _isFindUI = false;
        /// <summary>
        /// UI 검색
        /// </summary>
        private void FindUI()
        {
            if (true == _isFindUI)
                return;

            _itemsDic = new Dictionary<Type_Prob, UIProbItem>();
            FindProb(Type_Prob.LeftEye, "Prob_LeftEye");
            FindProb(Type_Prob.RightEye, "Prob_RightEye");
            FindProb(Type_Prob.Mouse, "Prob_Mouse");
            FindProb(Type_Prob.Face, "Prob_Face");
            FindProb(Type_Prob.SmallHat, "Prob_SmallHat");
            FindProb(Type_Prob.BigHat, "Prob_BigHat");

            _isFindUI = true;
        }

        private void FindProb(Type_Prob type, string gameobjectName)
        {
            var findItem = gameObject.GetComponent<UIProbItem>(gameobjectName);
            _itemsDic.Add(type, findItem);

            if(null == DataObject.Instance.ProbItems.Find(d => d.TBProb != null && d.TBProb.Type_Prob == type))
            {
                DataObject.Instance.ProbItems.Add(findItem);
            }
        }

        /// <summary>
        /// 캐릭터 데이터 셋팅 시작
        /// </summary>
        public void Initialize()
        {
            FindUI();

            foreach (KeyValuePair<Type_Prob, UIProbItem> slot in _itemsDic)
                slot.Value.gameObject.SetActive(false);
            
            int stageIndex = DataObject.Instance.StageRefId.Value;
            TB_Stage tbStage = TBData.Instance.TB_Stage.FirstOrDefault(d => d.ID == stageIndex);
            if (null == tbStage)
                return;

            TB_Character tbCharacter = TBData.Instance.TB_Character.FirstOrDefault(d => d.ID == tbStage.Character_RefId);
            if (null == tbCharacter)
                return;

            SetProbData(Type_Prob.LeftEye, tbCharacter.ProbRefId_LeftEye);
            SetProbData(Type_Prob.RightEye, tbCharacter.ProbRefId_RightEye);
            SetProbData(Type_Prob.Mouse, tbCharacter.ProbRefId_Mouse);
            SetProbData(Type_Prob.Face, tbCharacter.ProbRefId_Face);
            SetProbData(Type_Prob.SmallHat, tbCharacter.ProbRefId_SmallHat);
            SetProbData(Type_Prob.BigHat, tbCharacter.ProbRefId_BigHat);

            if (null != _dispose)
            {
                _dispose.Dispose();
                _dispose = null;
            }

            _dispose = DataObject.Instance.SelectedBodys.ObserveCountChanged().Subscribe( d =>
            {
                foreach(KeyValuePair<Type_Prob, UIProbItem> item in _itemsDic)
                {
                    item.Value.ChangeSelectBodys();
                }
            }).AddTo(this);
        }

        private void OnDisable()
        {
            if (null != _dispose)
            {
                _dispose.Dispose();
                _dispose = null;
            }
        }

        private void SetProbData(Type_Prob type, int probRefId)
        {
            if (false == _itemsDic.ContainsKey(type))
                return;

            _itemsDic[type].Initialize(probRefId);
        }
    }
}