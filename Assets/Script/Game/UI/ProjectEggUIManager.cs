﻿using ProjectEgg.Game;
using ProjectEgg.Lobby;
using ProjectEgg.Util;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class ProjectEggUIManager : SingletonMonobehaviour<ProjectEggUIManager>
{
    /// <summary>
    /// 윈도우 리스트
    /// </summary>
    private Dictionary<string, ProjectEggBaseWindow> _windowList;

    /// <summary>
    /// Canvas GameObject
    /// </summary>
    public GameObject _windowParent;

    private void OnEnable()
    {
        _windowList = new Dictionary<string, ProjectEggBaseWindow>();
        
        OpenWindow("UILoading");
    }

    /// <summary>
    /// Create window by Name
    /// </summary>
    /// <param name="uiName"></param>
    /// <returns></returns>
    private ProjectEggBaseWindow CreateWindow(string uiName)
    {
        string lowFileName = uiName.ToLower();
        string path = string.Empty;

        GameObject prefab = Resources.Load(string.Format("Prefab/{0}", uiName)) as GameObject;
        if (null == prefab)
            return null;

        GameObject instance = Instantiate(prefab) as GameObject;
        instance.SetActive(false);
        instance.name = prefab.name;
        instance.transform.position = Vector3.zero;
        instance.transform.localScale = Vector3.one;
        instance.transform.SetParent(_windowParent.gameObject.transform, false);
        ProjectEggBaseWindow window = instance.GetComponent<ProjectEggBaseWindow>();

        _windowList.Add(uiName, window);

        return window;
    }

    /// <summary>
    /// Window Open
    /// </summary>
    /// <param name="uiName"></param>
    public void OpenWindow(string uiName)
    {
        ProjectEggBaseWindow window = null;

        if (_windowList.TryGetValue(uiName, out window) == false)
        {
            window = CreateWindow(uiName);
        }
        
        if (window.gameObject.activeSelf == false)
        {
            window.gameObject.SetActive(true);
        }

        int index = GetTopSiblingIndex();
        if (index == -1)
            window.transform.SetAsLastSibling();
        else
            window.transform.SetSiblingIndex(index);
    }

    /// <summary>
    /// Window Close
    /// </summary>
    /// <param name="uiName"></param>
    public void CloseWindow(string uiName)
    {
        ProjectEggBaseWindow currentWindow = null;
        if (_windowList.TryGetValue(uiName, out currentWindow) == true)
        {
            currentWindow.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// 마지막 윈도우 레이어 가져오기
    /// </summary>
    /// <returns></returns>
    private int GetTopSiblingIndex()
    {
        var list = _windowList.Values.ToList();
        
        if (list.Count == 0)
            return -1;

        list.OrderByDescending(d => d.transform.GetSiblingIndex());
        return list[0].transform.GetSiblingIndex();
    }
}