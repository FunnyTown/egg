﻿using ProjectEgg.TableData;
using System;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectEgg.Game
{
    public class UICharacter : MonoBehaviour
    {
        /// <summary>
        /// Prob Body 아이템
        /// </summary>
        private GameObject _objProbBody { get; set; }

        /// <summary>
        /// 픽셀컬러로 색상을 변경할 Image
        /// </summary>
        private UIChangeBody _changeBody { get; set; }

        /// <summary>
        /// 정답 오브젝트
        /// </summary>
        public UIComplete UIComplete;

        /// <summary>
        /// 타이머 2종 Disposable
        /// </summary>
        private IDisposable _dispose_color;
        private IDisposable _dispose_Probitem;

        private bool _isFindUI = false;
        private void FindUI()
        {
            if (true == _isFindUI)
                return;
            
            var findBodys = gameObject.GetComponentsInChildren<UIProbBody>();
            if (null != findBodys)
            {
                foreach (var find in findBodys)
                {
                    if (find.gameObject.name == "ProbBody")
                        continue;

                    DataObject.Instance.ProbBodys.Add(find);
                }
            }

            _objProbBody = gameObject.GetChildObject("ProbBody");
            _changeBody = gameObject.GetComponent<UIChangeBody>("UIChangeBody");

            _isFindUI = true;
        }

        /// <summary>
        /// 캐릭터 데이터 셋팅 시작
        /// </summary>
        public void Initialize()
        {
            FindUI();

            var bodys = DataObject.Instance.ProbBodys;
            foreach (var slot in bodys)
                slot.gameObject.SetActive(false);
            
            _objProbBody.SetActive(false);
            
            int stageIndex = DataObject.Instance.StageRefId.Value;
            TB_Stage tbStage = TBData.Instance.TB_Stage.FirstOrDefault(d => d.ID == stageIndex);
            if (null == tbStage)
                return;
            
            TB_Character tbCharacter = TBData.Instance.TB_Character.FirstOrDefault(d => d.ID == tbStage.Character_RefId);
            if (null == tbCharacter)
                return;

            // 메인 몸통은 따로 해줌
            _changeBody.Initialize(tbCharacter.ProbRefId_Body);

            // 그 외에 프랍들만 생성
            SetBodyData(tbCharacter.ProbRefId_LeftEye);
            SetBodyData(tbCharacter.ProbRefId_RightEye);
            SetBodyData(tbCharacter.ProbRefId_Mouse);
            SetBodyData(tbCharacter.ProbRefId_Face);
            SetBodyData(tbCharacter.ProbRefId_SmallHat);
            SetBodyData(tbCharacter.ProbRefId_BigHat);

            if (null != _dispose_color)
            {
                _dispose_color.Dispose();
                _dispose_color = null;
            }
            if (null != _dispose_Probitem)
            {
                _dispose_Probitem.Dispose();
                _dispose_Probitem = null;
            }

            // 색상 선택 감지
            _dispose_color = DataObject.Instance.LastSelectColor.ObserveEveryValueChanged(v => v.Value).Subscribe((color) =>
            {
                _changeBody.ChangeColor();
                DataObject.Instance.ClearCheck();
            }).AddTo(this);
            
            // 프랍 아이템 선택 감지
            _dispose_Probitem = DataObject.Instance.SelectedBodys.ObserveCountChanged().Subscribe( d =>
            {
                SelectProbItem();
                DataObject.Instance.ClearCheck();
            }).AddTo(this);
        }
        
        /// <summary>
        /// 캐릭터 몸 셋팅
        /// </summary>
        /// <param name="ProbRefId"></param>
        private void SetBodyData(int ProbRefId)
        {
            if (ProbRefId < 1)
                return;

            UIProbBody findSlot = DataObject.Instance.ProbBodys.Find(d => d.gameObject.activeSelf == false);
            if(null != findSlot)
            {
                findSlot.Initialize(ProbRefId);
            }
            else
            {
                GameObject newBody = Instantiate(_objProbBody);
                newBody.transform.SetParent(transform);
                newBody.transform.localScale = Vector3.one;
                newBody.transform.SetAsLastSibling();

                findSlot = newBody.GetComponent<UIProbBody>();
                DataObject.Instance.ProbBodys.Add(findSlot);

                findSlot.Initialize(ProbRefId);
            }
        }

        /// <summary>
        /// 프랍 아이템이 선택됨
        /// </summary>
        public void SelectProbItem()
        {
            foreach (UIProbBody body in DataObject.Instance.ProbBodys)
            {
                body.SelectedBodyChange();
            }
        }

        /// <summary>
        /// Help 플레이 시작
        /// </summary>
        public void ShowHelp()
        {
            _changeBody.PlayHelp();
        }

        private void OnDisable()
        {
            if (null != _dispose_color)
            {
                _dispose_color.Dispose();
                _dispose_color = null;
            }
            if (null != _dispose_Probitem)
            {
                _dispose_Probitem.Dispose();
                _dispose_Probitem = null;
            }
        }
    }
}