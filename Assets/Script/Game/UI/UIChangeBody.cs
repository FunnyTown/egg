﻿using ProjectEgg.Resource;
using ProjectEgg.Util;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using ProjectEgg.TableData;
using System;
using System.Collections.Generic;
using System.Collections;
using UniLinq;

namespace ProjectEgg.Game
{
    public class UIChangeBody : MonoBehaviour
    {
        public List<UIProbBody> Bodys;

        /// <summary>
        /// 데이터
        /// </summary>
        private TB_Prob _tbProb { get; set; }

        /// <summary>
        /// 몸통 이미지
        /// </summary>
        private Image _image_BodyImage;

        /// <summary>
        /// 로드해서 사용한 텍스쳐
        /// </summary>
        //private Texture2D _loadTexture;

        /// <summary>
        /// 기존 원본 이미지 컬러
        /// </summary>
        Color32[] _defaultColor;

        private bool _isFindUI = false;
        private void FindUI()
        {
            if (true == _isFindUI)
                return;

            _image_BodyImage = GetComponent<Image>();

            _isFindUI = true;
        }

        /// <summary>
        /// 생성
        /// </summary>
        Sprite _newSprite;
        public void Initialize(int ProbRefId)
        {
            _tbProb = TBData.Instance.TB_Prob.Find(d => d.ID == ProbRefId);
            if (null == _tbProb)
            {
                gameObject.SetActive(false);
                return;
            }
            
            FindUI();
            gameObject.SetActive(true);

            // Position
            Vector3 localPosition = new Vector3(_tbProb.PositionX, _tbProb.PositionY, 0);
            gameObject.transform.localPosition = localPosition;
            
            // ProbImage
            _image_BodyImage.sprite = SpriteHolder.Instance.GetCharacter(_tbProb.ImageName);
            _image_BodyImage.SetNativeSize();

            Texture2D loadTexture;
            loadTexture = Resources.Load(string.Format("Character/{0}", _tbProb.ImageName)) as Texture2D;
            if (null == loadTexture)
                return;

            int width = (int)_image_BodyImage.rectTransform.GetWidth();
            int height = (int)_image_BodyImage.rectTransform.GetHeight();

            if (null == _newSprite)
                _newSprite = Sprite.Create(loadTexture, _image_BodyImage.sprite.rect, _image_BodyImage.sprite.pivot);

            Resources.UnloadAsset(loadTexture);
            loadTexture = null;

            _image_BodyImage.sprite = _newSprite;
            _defaultColor = _newSprite.texture.GetPixels32();
        }
        
        /// <summary>
        /// Character Prob 색상 변경
        /// </summary>
        public void ChangeColor()
        {
            int width = (int)_image_BodyImage.rectTransform.GetWidth();
            int height = (int)_image_BodyImage.rectTransform.GetHeight();

            // 선택중인 프랍을 가져옴
            List<UIProbBody> selects = new List<UIProbBody>();
            foreach (UIProbBody body in DataObject.Instance.ProbBodys)
            {
                if(true == DataObject.Instance.SelectedBodys.Contains(body.ProbType))
                {
                    selects.Add(body);
                }
            }
            
            // 마지막 선택 색상
            Color color = DataObject.Instance.LastSelectColor.Value.HexToColor();

            // 이전 색상
            Color32[] originColor = new Color32[width * height];
            originColor = _newSprite.texture.GetPixels32(0);

            // 덮어 씌울 색상 - 마지막에 선택한 컬러
            Color32[] newColor = new Color32[width * height];
            for (int i = 0; i < newColor.Length; i++)
            {
                if (originColor[i].a == 0)
                {
                    newColor[i].a = 0;
                    continue;
                }

                newColor[i] = color;
            }

            // 선택중인 프랍의 부분을 '기존의색상' 으로 대체하는중
            foreach (var selectProb in selects)
            {
                var ProbTexture = Resources.Load(string.Format("Character/{0}", selectProb.ImageName)) as Texture2D;

                // 선택한 이미지의 색상
                Color32[] probColor = ProbTexture.GetPixels32(0);
                int probColorIndex = 0;
                int change_height_max = selectProb.ColorPos_y + (int)selectProb.Size_Height;
                int change_width_max = selectProb.ColorPos_x + (int)selectProb.Size_Width;

                for (int j = selectProb.ColorPos_y; j < change_height_max; j++)
                {
                    for (int i = selectProb.ColorPos_x; i < change_width_max; i++)
                    {
                        if (probColor.Length < probColorIndex)
                            continue;

                        Color32 pixel = probColor[probColorIndex];
                        if (pixel.a != 0)
                        {
                            newColor[j * width + i] = originColor[j * width + i];
                        }
                        probColorIndex++;
                    }
                }

                Resources.UnloadAsset(ProbTexture);
                ProbTexture = null;
            }

            _newSprite.texture.SetPixels32(newColor);
            _newSprite.texture.Apply();
            DataObject.Instance.Character_Color = newColor;
        }

        /// <summary>
        /// 헬프 연출 시작
        /// </summary>
        public void PlayHelp()
        {
            DataObject.Instance.SelectedBodys.Clear();
            _newSprite.texture.SetPixels32(_defaultColor);

            StartCoroutine(PlayHelpRoutine());
        }
        IEnumerator PlayHelpRoutine()
        {
            DataObject.Instance.IsPlayHelp = true;

            TB_Stage tbStage = TBData.Instance.TB_Stage.FirstOrDefault(d => d.ID == DataObject.Instance.StageRefId.Value);

            TB_Color tbColor = TBData.Instance.TB_Color.FirstOrDefault(d => d.ID == tbStage.Color_RefId);

            string stringHelp = string.Empty;
            if(true == EditorDataObject.Instance.IsEditorMode.Value)
            {
                string log = string.Empty;
                var collection = EditorDataObject.Instance.ClickLog;
                foreach (string item in collection)
                {
                    log = string.Format("{0}{1}", log, item);
                }
                stringHelp = log;
            }
            else
            {
                stringHelp = tbStage.String_Help;
            }

            if (string.IsNullOrEmpty(stringHelp))
            {
                DataObject.Instance.IsPlayHelp = false;
                yield break;
            }

            List<string> listCompleteString = new List<string>();
            foreach (string value in stringHelp.Split(';').ToArray())
            {
                listCompleteString.Add(value);
            }
            
            if (null == tbColor)
                yield break;

            int bodyId;
            foreach (string data in listCompleteString)
            {
                // 값변경 실패 = 컬러값 선택
                if (int.TryParse(data, out bodyId) == false)
                {
                    switch (data)
                    {
                        case "C1": DataObject.Instance.LastSelectColor.Value = tbColor.Color1; break;
                        case "C2": DataObject.Instance.LastSelectColor.Value = tbColor.Color2; break;
                        case "C3": DataObject.Instance.LastSelectColor.Value = tbColor.Color3; break;
                        case "C4": DataObject.Instance.LastSelectColor.Value = tbColor.Color4; break;
                        default: break;
                    }
                }
                // 프랍 선택
                else
                {
                    TB_Prob tbProb = TBData.Instance.TB_Prob.Find(d => d.ID == bodyId);
                    if (null != tbProb)
                    {
                        UIProbItem findItem = DataObject.Instance.ProbItems.Find(d => d.TBProbRefId == bodyId);
                        if (null != findItem)
                            findItem.ProbItemTween();
                    }
                }

                yield return new WaitForSeconds(0.5f);
            }


            DataObject.Instance.IsPlayHelp = false;
        }

        /// <summary>
        /// 종료 후 리소스 리턴
        /// </summary>
        private void OnDisable()
        {
            if (false == _isFindUI)
                return;

            //if (null != _loadTexture)
            //{
            //    _loadTexture.SetPixels32(_defaultColor, 0);
            //    _loadTexture.Apply();
            //    _loadTexture = null;
            //}

            if (null != _image_BodyImage.sprite)
            {
                Destroy(_image_BodyImage.sprite);
            }
        }
    }
}