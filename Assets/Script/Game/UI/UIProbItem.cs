﻿using ProjectEgg.Resource;
using ProjectEgg.Util;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System;
using ProjectEgg.TableData;

namespace ProjectEgg.Game
{
    public class UIProbItem : MonoBehaviour
    {
        /// <summary>
        /// TB Data
        /// </summary>
        public TB_Prob TBProb { get; set; }

        /// <summary>
        /// Image
        /// </summary>
        private Image _image_Item;

        /// <summary>
        /// Image Shadow
        /// </summary>
        private Image _image_ItemBack;

        /// <summary>
        /// Tween
        /// </summary>
        private Jun_TweenRuntime _moveTween;

        /// <summary>
        /// shadow Tween
        /// </summary>
        private Jun_TweenRuntime _shadowTween;

        private Vector3 _defaultScale;

        public int TBProbRefId;
        private bool _isfindUI = false;
        private void FindUI()
        {
            if (true == _isfindUI)
                return;

            _image_Item = gameObject.GetComponent<Image>("Image_Item");
            _moveTween = _image_Item.GetComponent<Jun_TweenRuntime>();

            _image_ItemBack = gameObject.GetComponent<Image>("Image_ItemBack");
            _shadowTween = _image_ItemBack.GetComponent<Jun_TweenRuntime>();

            var findBtn = gameObject.GetComponent<Button>();
            findBtn.onClick.AddListener(OnClick_Prob);

            _isfindUI = true;
        }

        /// <summary>
        /// 데이터 셋팅
        /// </summary>
        /// <param name="probRefId"></param>
        public void Initialize(int probRefId)
        {
            TBProb = TBData.Instance.TB_Prob.Find(d => d.ID == probRefId);
            if(null == TBProb)
            {
                gameObject.SetActive(false);
                return;
            }

            TBProbRefId = probRefId;

            FindUI();
            gameObject.SetActive(true);
            
            _image_Item.sprite = SpriteHolder.Instance.GetCharacter(TBProb.ImageName);
            _image_ItemBack.sprite = SpriteHolder.Instance.GetCharacter(TBProb.ImageName);
            _defaultScale = _image_Item.rectTransform.localScale;
            isChangeScale = ( _defaultScale.x + _defaultScale.y + _defaultScale.z ) < 3;

            if (null != _moveTween)
            {
                if(_image_Item.transform.parent.gameObject != gameObject)
                {
                    _image_Item.transform.SetParent(gameObject.transform);
                    _image_Item.rectTransform.localPosition = Vector2.zero;

                    //if (true == isChangeScale)
                    //    _image_Item.rectTransform.localScale = new Vector2(0.5f, 0.5f);
                    //else
                        _image_Item.rectTransform.localScale = new Vector2(1, 1);
                }

                _moveTween.ClearOnFinished();
                _moveTween.AddOnFinished(TweenFinish);
            }

            ChangeSelectBodys();
        }

        /// <summary>
        /// 프랍 선택
        /// </summary>
        public void OnClick_Prob()
        {
            if (DataObject.Instance.IsPlayHelp)
                return;
            if (_moveTween != null && _moveTween.isPlaying)
                return;

            ProbItemTween();
        }

        bool isChangeScale = false;
        /// <summary>
        /// Tween
        /// </summary>
        /// <param name="reverse"></param>
        public void ProbItemTween()
        {
            // ProbBody
            var probBody = DataObject.Instance.ProbBodys.Find(d => d.TBProbRefId == TBProb.ID);

            // 선택중인경우 다시 선택 -> 원상복구
            if (true == DataObject.Instance.SelectedBodys.Contains(TBProb.Type_Prob))
            {
                _image_Item.transform.SetParent(gameObject.transform);

                _moveTween.GetTween(0).fromVector = new Vector2(0, 0);
                _moveTween.GetTween(0).toVector = _image_Item.rectTransform.localPosition;

                if (true == isChangeScale)
                {
                    _moveTween.GetTween(1).fromVector = _defaultScale;
                }
                else
                {
                    _moveTween.GetTween(1).fromVector = new Vector2(1f, 1f);
                }
                    
                _moveTween.GetTween(1).toVector = new Vector2(1f, 1f);

                // tween Play
                _moveTween.Rewind();
                _shadowTween.Rewind();

                DataObject.Instance.SelectedBodys.Remove(TBProb.Type_Prob);
            }
            // 선택 -> 덧씌워줌
            else
            {
                _image_Item.transform.SetParent(probBody.transform);

                _moveTween.GetTween(0).fromVector = _image_Item.rectTransform.localPosition;
                _moveTween.GetTween(0).toVector = new Vector2(probBody.Size_Width * 0.5f, probBody.Size_Height * 0.5f);

                if (true == isChangeScale)
                {
                    _moveTween.GetTween(1).fromVector = _image_Item.rectTransform.localScale;
                }
                else
                {
                    _moveTween.GetTween(1).fromVector = new Vector2(1f, 1f);
                }

                _moveTween.GetTween(1).toVector = new Vector2(1f, 1f);

                // tween play
                _moveTween.Play();
                _shadowTween.Play();

                DataObject.Instance.SelectedBodys.Add(TBProb.Type_Prob);
            }
            
            if(EditorDataObject.Instance.IsEditorMode.Value)
                EditorDataObject.Instance.ClickLog.Add(string.Format(";{0}", TBProb.ID.ToString()));
        }

        /// <summary>
        /// 프랍 변경 감지
        /// </summary>
        public void ChangeSelectBodys()
        {
            if (null == _image_Item)
                return;

            //_image_Item.enabled = !DataObject.Instance.SelectedBodys.Contains(_tbdProb.TypeProb);
        }

        private void TweenFinish()
        {
            //_image_Item.enabled = !DataObject.Instance.SelectedBodys.Contains(_tbdProb.TypeProb);
        }
    }
}