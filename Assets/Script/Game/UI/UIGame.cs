﻿using UniRx;
using System;
using UnityEngine.UI;

namespace ProjectEgg.Game
{
    using ProjectEgg.EditorMode;
    using ProjectEgg.TableData;
    using UniLinq;
    using UnityEngine;

    public class UIGame : ProjectEggBaseWindow
    {
        /// <summary>
        /// 캐릭터
        /// </summary>
        private UICharacter _character { get; set; }

        /// <summary>
        /// Prob Items
        /// </summary>
        private UIProbParent _probParent { get; set; }

        /// <summary>
        /// Color Items
        /// </summary>
        private UIColorParent _colorParent { get; set; }

        /// <summary>
        /// 정답
        /// </summary>
        private UIComplete _complete { get; set; }

        /// <summary>
        /// Editor 모드
        /// </summary>
        private UIEditorMode _editorMode { get; set; }

        /// <summary>
        /// 스테이지
        /// </summary>
        private Text _text_Stage { get; set; }

        /// <summary>
        /// 이전 스테이지 버튼
        /// </summary>
        private Button _button_Prev { get; set; }

        /// <summary>
        /// 다음 스테이지 버튼
        /// </summary>
        private Button _button_Next { get; set; }

        /// <summary>
        /// 클리어 UI
        /// </summary>
        private GameObject _clearObj { get; set; }

        /// <summary>
        /// 클리어 여부 dispose
        /// </summary>
        private IDisposable _dispose_clearCheck;

        private bool _isFindUI = false;
        /// <summary>
        /// UI 검색
        /// </summary>
        protected override void FindUI()
        {
            if (true == _isFindUI)
                return;

            _character = gameObject.GetComponent<UICharacter>("UIMainCharacter");
            _probParent = gameObject.GetComponent<UIProbParent>("UIProbItems");
            _colorParent = gameObject.GetComponent<UIColorParent>("UIColorItems");
            _complete = gameObject.GetComponent<UIComplete>("UIComplete");
            _clearObj = gameObject.GetChildObject("Clear");

            _editorMode = gameObject.GetComponent<UIEditorMode>("UIEditorMode");
            _editorMode.GameParent = this;

            _text_Stage = gameObject.GetComponent<Text>("Text_StageIndex");

            var findBtn = gameObject.GetComponent<Button>("Button_Help");
            findBtn.onClick.AddListener(ShowHelp);

            findBtn = gameObject.GetComponent<Button>("Button_Reset");
            findBtn.onClick.AddListener(GameClear);

            _button_Prev = gameObject.GetComponent<Button>("Button_Prev");
            _button_Prev.onClick.AddListener(OnClick_PrevStage);
        
            _button_Next = gameObject.GetComponent<Button>("Button_Next");
            _button_Next.onClick.AddListener(OnClick_NextStage);

            _isFindUI = true;
        }

        /// <summary>
        /// UI 활성화
        /// </summary>
        protected override void OnEnable()
        {
            OpenEvent = OpenEnd;
            base.OnEnable();
        }

        /// <summary>
        /// Base 활성화 오픈 이후 들어옴
        /// </summary>
        private void OpenEnd()
        {
            // UI 검색
            FindUI();

            // 플레이 데이터 리셋
            DataObject.Instance.Clear();
            
            // 캐릭터 셋팅
            _character.Initialize();
            _character.gameObject.SetActive(true);

            // 캐릭터 오브젝트 아이템
            _probParent.Initialize();
            _probParent.gameObject.SetActive(true);

            // 컬러
            _colorParent.Initialize();
            _colorParent.gameObject.SetActive(true);

            // 정답
            _complete.Initialize();
            _complete.gameObject.SetActive(true);

            // 스테이지 셋팅
            SetStageUI();

            // EditorMode 셋팅
            SetEditorMode();

            // 정답 여부
            _clearObj.SetActive(false);
            _dispose_clearCheck = DataObject.Instance.IsClear.ObserveEveryValueChanged(v => v.Value).Subscribe((clear) =>
            {
                _clearObj.SetActive(DataObject.Instance.IsClear.Value);
            }).AddTo(this);
        }

        /// <summary>
        /// 스테이지 정보 삽입
        /// </summary>
        private void SetStageUI()
        {
            int maxStageCount = TBData.Instance.TB_Stage.Count;
            int nowStage = DataObject.Instance.StageRefId.Value;

            var stages = TBData.Instance.TB_Stage.OrderBy(d => d.ID).ToList();
            int firstStageID = stages[0].ID;
            int lastStageID = stages[stages.Count - 1].ID;

            _text_Stage.SetText(string.Format("{0} / {1}", nowStage, maxStageCount));

            _button_Prev.interactable = !(firstStageID == nowStage);
            _button_Next.interactable = !(lastStageID == nowStage);
        }

        /// <summary>
        /// 에디터 모드 
        /// </summary>
        private void SetEditorMode()
        {
#if UNITY_EDITOR
            EditorDataObject.Instance.IsEditorMode.Value = true;
            _editorMode.gameObject.SetActive(true);
            _editorMode.Initialize();
#else
            EditorDataObject.Instance.IsEditorMode.Value = false;
            _editorMode.gameObject.SetActive(false);
#endif
        }

        /// <summary>
        /// 헬프 요청
        /// </summary>

        public Text TestText;
        public void ShowHelp()
        {
            TestText.SetText("");
            AdmobManager.Instance.ShowRewardMovieAd( (resString) =>
            {
#if UNITY_EDITOR
                _character.ShowHelp();
#else
                if(resString == "OnUserEarnedReward")
                    _character.ShowHelp();

                TestText.SetText($"+{TestText.text}{resString}");
#endif
            });
        }

        /// <summary>
        /// 초기화
        /// </summary>
        public void GameClear()
        {
            OpenEnd();
        }

        /// <summary>
        /// 이전 스테이지
        /// </summary>
        public void OnClick_PrevStage()
        {
            if (DataObject.Instance.IsPlayHelp)
                return;

            int nowStage = DataObject.Instance.StageRefId.Value;
            var find = TBData.Instance.TB_Stage.Find(d => d.ID == nowStage - 1);
            if (null == find)
                return;

            DataObject.Instance.StageRefId.Value = find.ID;
            DataObject.Instance.SaveCurrentStage();
            GameClear();
        }

        /// <summary>
        /// 다음 스테이지
        /// </summary>
        public void OnClick_NextStage()
        {
            if (DataObject.Instance.IsPlayHelp)
                return;

            AdmobManager.Instance.ShowScreenAdBanner(() =>
            {
                int nowStage = DataObject.Instance.StageRefId.Value;
                var find = TBData.Instance.TB_Stage.Find(d => d.ID == nowStage + 1);
                if (null == find)
                    return;

                DataObject.Instance.StageRefId.Value = find.ID;
                DataObject.Instance.SaveCurrentStage();
                GameClear();
            });
        }

        private void OnDisable()
        {
            if (null != _dispose_clearCheck)
            {
                _dispose_clearCheck.Dispose();
                _dispose_clearCheck = null;
            }
        }
    }
}