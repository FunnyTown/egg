﻿using ProjectEgg.Resource;
using ProjectEgg.TableData;
using ProjectEgg.Util;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectEgg.Game
{
    public class UIComplete : MonoBehaviour
    {
        /// <summary>
        /// 데이터
        /// </summary>
        private TB_Prob _tbProb { get; set; }

        /// <summary>
        /// 몸통 이미지
        /// </summary>
        private Image _image_BodyImage;

        /// <summary>
        /// 프랍리스트
        /// </summary>
        private List<Type_Prob> _list_typeProb = new List<Type_Prob>();

        private Sprite _newSprite;

        /// <summary>
        /// 정답으로 사용할 텍스쳐
        /// </summary>
        //private Texture2D _loadTexture;

        /// <summary>
        /// 기존 원본 이미지 컬러
        /// </summary>
        Color32[] _defaultColor;

        private bool _isFindUI = false;
        private void FindUI()
        {
            if (true == _isFindUI)
                return;

            _image_BodyImage = GetComponent<Image>();

            _isFindUI = true;
        }

        public void Clear()
        {

        }

        /// <summary>
        /// 생성
        /// </summary>
        /// <param name="ProbRefId"></param>
        public void Initialize()
        {
            FindUI();
            _list_typeProb.Clear();

            int stageIndex = DataObject.Instance.StageRefId.Value;
            TB_Stage tbStage = TBData.Instance.TB_Stage.FirstOrDefault(d => d.ID == stageIndex);
            if (null == tbStage)
                return;

            TB_Character tbCharacter = TBData.Instance.TB_Character.FirstOrDefault(d => d.ID == tbStage.Character_RefId);
            if (null == tbCharacter)
                return;

            _tbProb = TBData.Instance.TB_Prob.Find(d => d.ID == tbCharacter.ProbRefId_Body);
            if (null == _tbProb)
            {
                gameObject.SetActive(false);
                return;
            }

            // ProbImage
            _image_BodyImage.sprite = SpriteHolder.Instance.GetCharacter(_tbProb.ImageName);
            _image_BodyImage.SetNativeSize();

            var loadTexture = Resources.Load(string.Format("CompleteBody/{0}", _tbProb.ImageName)) as Texture2D;

            int width = (int)_image_BodyImage.rectTransform.GetWidth();
            int height = (int)_image_BodyImage.rectTransform.GetHeight();

            if(null == _newSprite)
                _newSprite = Sprite.Create(loadTexture, _image_BodyImage.sprite.rect, _image_BodyImage.sprite.pivot);

            Resources.UnloadAsset(loadTexture);
            loadTexture = null;

            _image_BodyImage.sprite = _newSprite;

            _defaultColor = _newSprite.texture.GetPixels32();

            _image_BodyImage.enabled = true;

            // 완성 결과 만들기 시작
            StartComplete(tbStage, tbCharacter);
        }

        /// <summary>
        /// 이미지 생성
        /// </summary>
        private void StartComplete(TB_Stage tbStage, TB_Character tbCharacter)
        {
            string complete = tbStage.String_Complete;

            List<string> listCompleteString = new List<string>();
            foreach (string value in complete.Split(';').ToArray())
            {
                listCompleteString.Add(value);
            }

            TB_Color tbColor = TBData.Instance.TB_Color.FirstOrDefault(d => d.ID == tbStage.Color_RefId);
            if (null == tbColor)
                return;

            int bodyId;
            foreach (string data in listCompleteString)
            {
                // 값변경 실패 = 컬러값 선택
                if (int.TryParse(data, out bodyId) == false)
                {
                    switch (data)
                    {
                        case "C1": SetColor(tbColor.Color1); break;
                        case "C2": SetColor(tbColor.Color2); break;
                        case "C3": SetColor(tbColor.Color3); break;
                        case "C4": SetColor(tbColor.Color4); break;
                        default: break;
                    }
                }
                // 프랍 선택
                else
                {
                    TB_Prob tbProb = TBData.Instance.TB_Prob.Find(d => d.ID == bodyId);
                    if (null != tbProb)
                    {
                        if (_list_typeProb.Contains(tbProb.Type_Prob))
                        {
                            _list_typeProb.Remove(tbProb.Type_Prob);
                        }
                        else
                        {
                            _list_typeProb.Add(tbProb.Type_Prob);
                        }
                    }
                }
            }
        }

        private void SetColor(string scolor)
        {
            int width = (int)_image_BodyImage.rectTransform.GetWidth();
            int height = (int)_image_BodyImage.rectTransform.GetHeight();

            // 선택중인 프랍을 가져옴
            List<UIProbBody> selects = new List<UIProbBody>();
            foreach (UIProbBody body in DataObject.Instance.ProbBodys)
            {
                if (true == _list_typeProb.Contains(body.ProbType))
                {
                    selects.Add(body);
                }
            }

            // 마지막 선택 색상
            Color color = scolor.HexToColor();

            // 이전 색상
            Color32[] originColor = new Color32[width * height];
            originColor = _newSprite.texture.GetPixels32(0);

            // 덮어 씌울 색상 - 마지막에 선택한 컬러
            Color32[] newColor = new Color32[width * height];
            for (int i = 0; i < newColor.Length; i++)
            {
                if (originColor[i].a == 0)
                {
                    newColor[i].a = 0;
                    continue;
                }

                newColor[i] = color;
            }

            // 선택중인 프랍의 부분을 '기존의색상' 으로 대체하는중
            foreach (var selectProb in selects)
            {
                var ProbTexture = Resources.Load(string.Format("Character/{0}", selectProb.ImageName)) as Texture2D;

                // 선택한 이미지의 색상
                Color32[] probColor = ProbTexture.GetPixels32(0);
                int probColorIndex = 0;
                int change_height_max = selectProb.ColorPos_y + (int)selectProb.Size_Height;
                int change_width_max = selectProb.ColorPos_x + (int)selectProb.Size_Width;

                for (int j = selectProb.ColorPos_y; j < change_height_max; j++)
                {
                    for (int i = selectProb.ColorPos_x; i < change_width_max; i++)
                    {
                        if (probColor.Length < probColorIndex)
                            continue;

                        Color32 pixel = probColor[probColorIndex];
                        if (pixel.a != 0)
                        {
                            newColor[j * width + i] = originColor[j * width + i];
                        }
                        probColorIndex++;
                    }
                }

                Resources.UnloadAsset(ProbTexture);
                ProbTexture = null;
            }

            _newSprite.texture.SetPixels32(newColor);
            _newSprite.texture.Apply();
            DataObject.Instance.Complete_Color = newColor;
        }

        /// <summary>
        /// 종료 후 리소스 리턴
        /// </summary>
        private void OnDisable()
        {
            if (false == _isFindUI)
                return;

            //if(null !=_loadTexture)
            //{
            //    _loadTexture.SetPixels32(_defaultColor, 0);
            //    _loadTexture.Apply();
            //    _loadTexture = null;
            //}

            if (null != _image_BodyImage.sprite)
            {
                Destroy(_image_BodyImage.sprite);
            }
        }
    }
}