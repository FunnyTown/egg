﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using ProjectEgg.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace ProjectEgg
{
    public class ProjectEggLoginManager : Singleton<ProjectEggLoginManager>
    {
        /// <summary>
        /// 로그인 상태 string
        /// </summary>
        public ReactiveProperty<string> LoginState { get; private set; }

        private Action<bool> signInCallback;

        public bool isWaitAuth;

        public ProjectEggLoginManager()
        {
            LoginState = new ReactiveProperty<string>();
            isWaitAuth = false;

            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);

            // 구글 플레이 로그를 확인할려면 활성화
            PlayGamesPlatform.DebugLogEnabled = true;

            // 구글 플레이 활성화
            PlayGamesPlatform.Activate();

            // Callback 함수 정의
            signInCallback = (bool success) =>
            {
                if (success)
                    LoginState.Value = "SignIn Success!";
                else
                    LoginState.Value = "SignIn Fail!";
            };
        }

        /// <summary>
        /// 자동로그인
        /// </summary>
        public void OnLogin_Auto_Google(Action<bool> loginReturn)
        {
            if (PlayGamesPlatform.Instance.IsAuthenticated() == false)
            {
                PlayGamesPlatform.Instance.Authenticate((isSuccess) =>
                {
                    if (true == isSuccess)
                        LoginState.Value = "SignIn Success!";
                    else
                        LoginState.Value = "SignIn Fail!";

                    loginReturn(isSuccess);
                });
            }
        }
        
        /// <summary>
        /// 로그아웃
        /// </summary>
        public void OnLogOut()
        {
            // 로그인 상태면 호출
            if (PlayGamesPlatform.Instance.IsAuthenticated() == true)
            {
                LoginState.Value = "Bye~!";
                PlayGamesPlatform.Instance.SignOut();
            }
        }
    }
}