﻿using ProjectEgg.Util;
using System.Collections.Generic;
using System.Resources;
using UnityEngine;
using UnityEngine.U2D;

namespace ProjectEgg.Common
{
    public class SpriteManager : SingletonMonobehaviour<SpriteManager>
    {
        private List<SpriteAtlas> ListSpriteAtlas { get; set; }

        protected override void Awake()
        {
            if (_inst != null)
            {
                Destroy(this);
                return;
            }

            base.Awake();
        }

        private SpriteAtlas GetAtlas(string atlasName)
        {
            if (ListSpriteAtlas == null)
                return null;

            ListSpriteAtlas.RemoveAll(d => d == null);

            if (ListSpriteAtlas.Exists(d => d.name == atlasName) == false)
            {
                ListSpriteAtlas.Add(Resources.Load<SpriteAtlas>(string.Format("Atlas/{0}", atlasName)));
            }

            return ListSpriteAtlas.Find(d => null != d && d.name == atlasName);
        }

        /// <summary>
        /// Get Sprite
        /// </summary>
        /// <param name="atlasName"></param>
        /// <param name="spriteName"></param>
        /// <returns></returns>
        private Sprite GetSprite(string atlasName, string spriteName)
        {
            if (string.IsNullOrEmpty(spriteName) == true)
                return null;

            SpriteAtlas atlas = GetAtlas(atlasName);
            if (null == atlas)
                return null;

            return atlas.GetSprite(spriteName);
        }

        /// <summary>
        /// Get Sprite by path Sprite/Icon/Character/
        /// </summary>
        /// <param name="prefab"></param>
        /// <returns></returns>
        public Sprite GetCharacterIcon(string prefab)
        {
            return GetSprite("Character", prefab);
        }
    }
}
