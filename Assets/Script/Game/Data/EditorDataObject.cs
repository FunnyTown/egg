﻿
using UniRx;
using UnityEngine;

namespace ProjectEgg.Game
{
    using ProjectEgg.Util;
    using System.Collections.Generic;

    public class EditorDataObject : Singleton<EditorDataObject>
    {
        public ReactiveProperty<bool> IsEditorMode = new ReactiveProperty<bool>();
        public ReactiveCollection<string> ClickLog = new ReactiveCollection<string>();
    }
}