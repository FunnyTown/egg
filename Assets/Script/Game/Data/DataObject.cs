﻿using UniRx;
using UnityEngine;

namespace ProjectEgg.Game
{
    using ProjectEgg.Util;
    using System.Collections.Generic;

    public class DataObject : Singleton<DataObject>
    {
        /// <summary>
        /// 선택중인 ProbDic
        /// </summary>
        public ReactiveCollection<Type_Prob> SelectedBodys = new ReactiveCollection<Type_Prob>();

        /// <summary>
        /// 마지막에 선택한 색상
        /// </summary>
        public ReactiveProperty<string> LastSelectColor = new ReactiveProperty<string>();

        /// <summary>
        /// 현재 선택중인 스테이지
        /// </summary>
        public ReactiveProperty<int> StageRefId = new ReactiveProperty<int>();

        /// <summary>
        /// 정답 여부
        /// </summary>
        public ReactiveProperty<bool> IsClear = new ReactiveProperty<bool>();

        /// <summary>
        /// 캐릭터 ProbBody
        /// </summary>
        public List<UIProbBody> ProbBodys = new List<UIProbBody>();

        /// <summary>
        /// 캐릭터 ProbBody
        /// </summary>
        public List<UIProbItem> ProbItems = new List<UIProbItem>();

        /// <summary>
        /// 캐릭터 컬러값
        /// </summary>
        public Color32[] Character_Color { get; set; }

        /// <summary>
        /// 정답 컬러값
        /// </summary>
        public Color32[] Complete_Color { get; set; }

        /// <summary>
        /// 도움말이 플레이 중인지.
        /// </summary>
        public bool IsPlayHelp { get; set; }

        public DataObject()
        {
            Clear();
        }

        /// <summary>
        /// 게임 초기화
        /// </summary>
        public void Clear()
        {
            SelectedBodys.Clear();
            IsClear.Value = false;
            LastSelectColor.Value = string.Empty;
            StageRefId.Value = 0;
            Character_Color = new Color32[1];
            Complete_Color = new Color32[1];
            IsPlayHelp = false;

            LoadCurrentStage();
        }
        
        /// <summary>
        /// 스테이지 번호 저장
        /// </summary>
        public void SaveCurrentStage()
        {
            PlayerPrefs.SetInt("CurrentStage", StageRefId.Value);
        }

        /// <summary>
        /// 스테이지 번호 로드
        /// </summary>
        public void LoadCurrentStage()
        {
            int stageId = PlayerPrefs.GetInt("CurrentStage");
            if (stageId < 1)
                stageId = 1;

            var findStage = TableData.TBData.Instance.TB_Stage.Find(d => d.ID == stageId);
            if (null == findStage)
                stageId = 1;

            StageRefId.Value = stageId;
        }

        private int[] _checkList_x = new int[] { 73, 78, 74, 131, 269, 131, 133 };
        private int[] _checkList_y = new int[] { 85, 98, 115, 120, 313, 165, 220 };

        /// <summary>
        /// 정답 체크
        /// </summary>
        /// <returns></returns>
        public void ClearCheck()
        {
            if (0 < SelectedBodys.Count)
                return;

            for(int i = 0; i < _checkList_x.Length; i++)
            {
                if (false == IsSamePixelColor(_checkList_x[i], _checkList_y[i]))
                {
                    IsClear.Value = false;
                    return;
                }
            }

            IsClear.Value = true;
        }

        /// <summary>
        /// 해당 픽셀의 컬러가 같은지 검사
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private bool IsSamePixelColor(int x , int y)
        {
            // 92, 65
            int pixel_x = 268 - x;
            pixel_x = pixel_x < 1 ? 1 : pixel_x;

            int pixel_y = 312 - y;
            pixel_y = pixel_y < 1 ? 1 : pixel_y;

            int index = pixel_x + ( pixel_y * 268 );

            if (Complete_Color.Length < 2)
                return false;

            if (Complete_Color.Length < index || Character_Color.Length < index)
                return false;

            if(    Complete_Color[index].r == Character_Color[index].r
                && Complete_Color[index].g == Character_Color[index].g
                && Complete_Color[index].b == Character_Color[index].b
                && Complete_Color[index].a == Character_Color[index].a)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 광고 제거를 구매했는지
        /// </summary>
        /// <returns></returns>
        public bool IsBuyDeleteAds()
        {
            return false;
        }
    }
}