﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectEgg.TableData
{
    using ProjectEgg.Util;
    public class TBData : SingletonMonobehaviour<TBData>
    {
        [SerializeField] TBProjectEgg Data;

        public List<TB_Stage> TB_Stage { get; private set; }
        public List<TB_Character> TB_Character { get; private set; }
        public List<TB_Color> TB_Color { get; private set; }
        public List<TB_Prob> TB_Prob { get; private set; }
        protected override void Awake()
        {
            base.Awake();

            TB_Stage = new List<TB_Stage>();
            foreach (TB_Stage item_stage in Data.TB_Stage)
                TB_Stage.Add(item_stage);

            TB_Character = new List<TB_Character>();
            foreach (TB_Character item_character in Data.TB_Character)
                TB_Character.Add(item_character);

            TB_Color = new List<TB_Color>();
            foreach (TB_Color item_color in Data.TB_Color)
                TB_Color.Add(item_color);

            TB_Prob = new List<TB_Prob>();
            foreach (TB_Prob item_prob in Data.TB_Prob)
                TB_Prob.Add(item_prob);
        }
    }

    /// <summary>
    /// Stage
    /// </summary>
    [Serializable]
    public class TB_Stage
    {
        public int ID;
        public int Character_RefId;
        public int Color_RefId;
        public string String_Complete;
        public string String_Help;
    }

    /// <summary>
    /// Character
    /// </summary>
    [Serializable]
    public class TB_Character
    {
        public int ID;
        public int ProbRefId_Body;
        public int ProbRefId_LeftEye;
        public int ProbRefId_RightEye;
        public int ProbRefId_Mouse;
        public int ProbRefId_Face;
        public int ProbRefId_SmallHat;
        public int ProbRefId_BigHat;
    }

    /// <summary>
    /// Color
    /// </summary>
    [Serializable]
    public class TB_Color
    {
        public int ID;
        public string Color1;
        public string Color2;
        public string Color3;
        public string Color4;
    }

    /// <summary>
    /// Prob
    /// </summary>
    [Serializable]
    public class TB_Prob
    {
        public int ID;
        public string ImageName;
        public int PositionX;
        public int PositionY;
        public Type_Prob Type_Prob;
    }
}