using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProjectEgg.TableData;

[ExcelAsset]
public class TBProjectEgg : ScriptableObject
{
	public List<TB_Stage> TB_Stage; // Replace 'EntityType' to an actual type that is serializable.
	public List<TB_Character> TB_Character; // Replace 'EntityType' to an actual type that is serializable.
	public List<TB_Color> TB_Color; // Replace 'EntityType' to an actual type that is serializable.
	public List<TB_Prob> TB_Prob; // Replace 'EntityType' to an actual type that is serializable.
}
